#!/bin/bash

function aws-mfa-init {
  echo "This will initialise the mfa ability per profile."
  echo "If you have multiple profiles, please run for each profile."
  echo -n "Please enter the profile name: "
    read profile
  echo -n "Please enter the user name: "
    read user
  echo -n "Please enter the MFA device serial: "
    read mfa_device_serial

  if [[ ${#profile} -eq 0 ]] \
  || [[ ${#user} -eq 0 ]] \
  || [[ ${#mfa_device_serial} -eq 0 ]]
    then
      echo -e "Missing data.\nprofile: $profile\nuser: $user\nmfa device serial: $mfa_device_serial"
      exit
  fi

  parse-aws-config

  # exit if the profile can't be found
  if ! $config_found
    then
      echo "The profile $profile has not been found in the aws config"
      echo "Without a preconfigured aws profile, this script can't continue."
      echo -n "Do you want to configure that aws profile now [Y/n]? "
      read yesno

      if [[ ${#yesno} -eq 0 ]] \
      || [[ "$yesno" == "Y" ]] \
      || [[ "$yesno" == "y" ]]
        then
          aws configure --profile $profile

        else
          exit
      fi
  fi

  parse-aws-config

  if ! $config_found
    then
      echo "The profile $profile has not been found in the aws config. Exiting now."
      exit
  fi

  # if old lines found replace them
  if [[ ${#config_user} -gt 0 ]] \
  || [[ ${#config_mfa_device_serial} -gt 0 ]]
    then
      sed_replace=""

      if [[ ${#config_user} -gt 0 ]]
        then
          sed_replace+="s/${config_user}/user = ${user}/;"
      fi

      if [[ ${#config_mfa_device_serial} -gt 0 ]]
        then
          sed_replace+="s/${config_mfa_device_serial}/mfa_device_serial = ${mfa_device_serial}/;"
      fi

      sed -i.bak "$sed_replace" ~/.aws/config

    # Otherwise insert the lines below the profile section
    else
      mv ~/.aws/config ~/.aws/config.bak
      head -n $line_number ~/.aws/config.bak > ~/.aws/config
      echo "user = ${user}" >> ~/.aws/config
      echo "mfa_device_serial = ${mfa_device_serial}" >> ~/.aws/config
      ((line_number++))
      tail -n +$line_number ~/.aws/config.bak >> ~/.aws/config
  fi
}

function process-arguments {
  if [[ $# -eq 1 ]]
    then
      profile="default"
      mfa_profile="default-mfa"
      mfa_code=$1
  fi

  if [[ $# -eq 2 ]]
    then
      profile=$1
      mfa_profile="${1}-mfa"
      mfa_code=$2
  fi

  if [[ ${#mfa_code} -eq 0 ]]
    then
      echo "Missing MFA CODE"
      exit
  fi
}

function write-aws-config {
  config_done=false
  while read line
    do
      if [[ "$line" == "[profile $mfa_profile]" ]]
        then
          config_done=true
          break
      fi
    done < ~/.aws/config

  if ! $config_done
    then
      echo "" >> ~/.aws/config
      echo "[profile $mfa_profile]" >> ~/.aws/config
      echo "output = json" >> ~/.aws/config
      echo "region = eu-west-1" >> ~/.aws/config
      echo "profile: $mfa_profile created in config"
  fi
}

function write-aws-credentials {
  config_found=false
  config_read=false
  while read line
    do
      # If it's another config block stop reading
      if [[ "${line:0:1}" == "[" ]]
        then
          config_read=false
      fi

      # Parsing
      if $config_read && [[ "${line:0:17}" == "aws_access_key_id" ]]
        then
          old_aws_access_key_id_line=$line
      fi

      # Parsing
      if $config_read && [[ "${line:0:21}" == "aws_secret_access_key" ]]
        then
          old_aws_secret_access_key_line=$line
      fi

      # Parsing
      if $config_read && [[ "${line:0:17}" == "aws_session_token" ]]
        then
          old_aws_session_token_line=$line
      fi

      # If it's our config block start reading
      if [[ "$line" == "[$mfa_profile]" ]]
        then
          config_found=true
          config_read=true
      fi
    done < ~/.aws/credentials

  if $config_found
    then
      if [[ ${#old_aws_access_key_id_line} -eq 0 ]] \
      || [[ ${#old_aws_secret_access_key_line} -eq 0 ]] \
      || [[ ${#old_aws_session_token_line} -eq 0 ]]
        then
          echo "Error: Old key id, secret, and token has not been found, exiting now."
          exit
      fi

      sed -i.bak "
          s/${old_aws_access_key_id_line//\//\/}/aws_access_key_id = ${aws_access_key_id//\//\/}/;
          s/${old_aws_secret_access_key_line//\//\/}/aws_secret_access_key = ${aws_secret_access_key//\//\/}/;
          s/${old_aws_session_token_line//\//\/}/aws_session_token = ${aws_session_token//\//\/}/;
        " ~/.aws/credentials

      echo "profile: $mfa_profile updated in credentials"

    else
      echo "[$mfa_profile]" >> ~/.aws/credentials
      echo "aws_access_key_id = $aws_access_key_id" >> ~/.aws/credentials
      echo "aws_secret_access_key = $aws_secret_access_key" >> ~/.aws/credentials
      echo "aws_session_token = $aws_session_token" >> ~/.aws/credentials
      echo "profile: $mfa_profile created in credentials"
  fi
}

function read-aws-config {
  IFS="="
  parsing=false
  while read -r name value
    do
      # Stop parsing if no value presented, which means an empty line, or a config directive
      if [[ ${#value} -eq 0 ]]
        then parsing=false
      fi

      # Parsing config values
      if $parsing
        then
          case ${name// /} in
            'user') user=${value// /};;
            'mfa_device_serial') mfa_device_serial=${value// /};;
          esac
      fi

      # Start parsing if we found the profile we looking for
      if [[ "$name" == "[profile $profile]" ]] || [[ "$name" == "[$profile]" ]]
        then parsing=true
      fi

    done < ~/.aws/config

  if [[ ${#user} -eq 0 ]] || [[ ${#mfa_device_serial} -eq 0 ]]
    then
      echo "Looks like it hasn't been set up yet, running init"
      aws-mfa-init
      exit
    else
      echo "Using user $user"
  fi
}

function parse-aws-config {
  # reading config
  config_found=false
  config_parse=false
  line_number=0
  read_line_number=0
  while read line
    do
      ((read_line_number++))

      if [[ "${line:0:1}" == "[" ]]
        then
          config_parse=false
      fi

      if $config_parse && [[ "${line:0:6}" == "output" ]]
        then config_output=$line
      fi

      if $config_parse && [[ "${line:0:6}" == "region" ]]
        then config_region=$line
      fi

      if $config_parse && [[ "${line:0:4}" == "user" ]]
        then config_user=$line
      fi

      if $config_parse && [[ "${line:0:17}" == "mfa_device_serial" ]]
        then config_mfa_device_serial=$line
      fi

      if [[ "$line" == "[profile $profile]" ]]
        then
          config_parse=true
          config_found=true
          line_number=$read_line_number
      fi
    done < ~/.aws/config
}
