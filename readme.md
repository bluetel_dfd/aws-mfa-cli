## MFA script for AWS cli

Supports multiple profiles.
All you need is to invoke mfa for every profile you have, and it will create an `-mfa` suffixed profile for each.
For example I have `dev-packt` and `prod-packt`.
I call `aws-cli dev-packt 123456` and it will create a profile called `dev-packt-mfa`. So I can use it like `aws --profile dev-packt-mfa s3 ls`.
Can be used with eb clie as well, but the version I got only supports profile at the end of the command, like `eb status -v --profile dev-packt-mfa`

Just a tip: you can create alias like `aws-dev-packt='aws --profile dev-packt-mfa'`
For my version of eb cli I created a function as an alias: `function eb-dev-packt{ eb $@ --profile dev-packt-mfa; }`

##### After cloning you may need to do the following steps to make it work:

1. Make the install executable as SU `sudo chmod +x install`
2. Run install from the script directory `./install`
3. For first run do `aws-mfa init` for setup
